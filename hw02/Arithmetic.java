/*Bradford DeMassa
5 February, 2018
CSE2
This program is meant to be a bill summary, including price of each item and include sales tax*/
public class Arithmetic {
  public static void main (String[] args) {
    
    int numPants = 3;              //Total number of pants 
    double pantsPrice = 34.98;     //Pants Price
    int numShirts = 2;             //Total number of shirts
    double shirtPrice = 24.99;     //Shirt Price 
    int numBelts = 1;              //Total number of belts
    double beltCost = 33.99;       //Belt Price
    double paSalesTax = .06;       //Pennsylvania Sales Tax
    
    double totalCostOfPants = (numPants * pantsPrice);                                //This block determines the total cost of pants, shirts, belts, and the untaxed total 
    double totalCostOfShirts = (numShirts * shirtPrice);
    double totalCostOfBelts = (numBelts * beltCost);
    double totalPurchase = totalCostOfBelts + totalCostOfShirts + totalCostOfPants;
    
    double pantsTax = totalCostOfPants * paSalesTax;          //This block determines the tax on each item, including the tax on the total bill
    double shirtTax = totalCostOfShirts * paSalesTax;
    double beltTax = totalCostOfBelts * paSalesTax;
    double totalTax = pantsTax + shirtTax + beltTax;
    
    double pantsTaxf = ((int)(pantsTax *100))/100.0;        //These are the semi-incorrect truncated tax values for each item
    double shirtTaxf = ((int)(shirtTax *100))/100.0;
    double beltTaxf = ((int)(beltTax *100))/100.0;
    double totalTaxf = ((int)(totalTax *100))/100.0;
    double total = totalTaxf + totalPurchase;        //This line is special, it does not need to be truncated. totalTaxf is already rounded to 2 places and so is totalPurchase
    
    System.out.println("The total cost of pants is $" + totalCostOfPants);   //Display of prices with their assoicated tax, with the totals at the bottom of the block
    System.out.println("The sales tax on pants is $" + pantsTaxf);
    System.out.println("");                                                  //Aesthetic for display and to break up totals and taxes into an easier read 
    System.out.println("The total cost of shirts is $" + totalCostOfShirts);
    System.out.println("The sales tax on shirts is $" + shirtTaxf);
    System.out.println("");
    System.out.println("The total cost of belts is $" + totalCostOfBelts);
    System.out.println("The sales tax on belts is $" + beltTaxf);
    System.out.println("");
    System.out.println("The total cost before tax is $" + totalPurchase);
    System.out.println("The total sales tax is $" + totalTaxf);
    System.out.println("The total cost is $" + total);   
  }//end of main method
}//end of class