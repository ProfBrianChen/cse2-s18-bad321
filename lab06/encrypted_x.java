/* Bradford DeMassa
CSE2
9 March 2018
This program is meant to ask for an input of an integer between 1 and 100 and
will print out a formation of stars with a hidden x, varying in size based on
the integer put in.*/
import java.util.Scanner;//scanner class
public class encrypted_x{
  public static void main (String[] args){
    Scanner num = new Scanner(System.in);//new scanner
    System.out.print("Enter an integer between 1 and 100:");
    boolean x = num.hasNextInt();//check to see if input is an integer
    while (x == false){
      String junk = num.next();//throw away input
      System.out.println("Error: Valid integer not entered.");
      System.out.print("Re-enter a valid integer between 1 and 100:");
      x = num.hasNextInt();//new input
    }//while if input is not an integer
    int input = num.nextInt();//integer input
    if (input<1 || input>100){
      System.out.println("Error: Valid integer not entered.");
      System.out.print("Re-enter a valid integer between 1 and 100:");
      x = num.hasNextInt();//check new input
    }//if number is not within specifications
    for (int i = 0 ; i<input; i++){
      for(int j = 0; j<input; j++){
       if(i==j){
         System.out.print(" ");
       }//
        else if (input-i==j+1){
          System.out.print(" ");
        }
        else {
        System.out.print("*");
        }
      }//inner for bracket
      System.out.println("");
     }//outer for bracket
  }//end main method
}//end of class