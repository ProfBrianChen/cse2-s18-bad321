/*
Bradford DeMassa
CSE 2
2 March 2018
This program is meant to generate a bunch of slashes and x's depending on user input for the twist length*/
import java.util.Scanner;                                                       //sets up scanner
public class TwistGenerator {                                                   //standard class required for all java code
  public static void main (String[] args){
    Scanner myScanner = new Scanner (System.in);//Constructs the scanner to recive input
    System.out.print("Enter a positive integer:");
    boolean x = myScanner.hasNextInt();// checks to see if the scanner has recieved an integer value
    while (x == false){
    String junkWord = myScanner.next();//if false, get rid of entered text
    System.out.print("Re-enter an integer:");
    x = myScanner.hasNextInt();//reassign scanner to look for an incoming integer, if false, run the loop again
    }
    int length = myScanner.nextInt();// set integer length equal to scanner integer
  int a = 0;//initalize all variables
  int b = 0;
  int c = 0;
  int y = 0;
  int z = 0;
  int v = 0;
  while (a<length){//loop for as many times as the length
    y = a % 3;//take remainder of the position by the three possible choices
    switch (y){
      case 0:// this prints out one slash
      System.out.print("\\");
      break;
      case 2://prints out other slash
      System.out.print(" /") ;   
      break;
//if no cases are meant for the specfic space leave the space blank
    }
     a++;//preincrement the counter of the loop
  }
  System.out.println("");//ends the calculation of the first line and moves on determining the x's position. 
  while (b<=length){
    z = b % 3;
    switch (z){
      case 2:
      System.out.print(" x ");
      break;
    }
      b++;
  }
  System.out.println("");//the second and third lines follow the pattern of loop a, just with different variable names
  while (c<length){
    v = c % 3;
    switch (v){
      case 0:
      System.out.print("/");
      break;
      case 2:
      System.out.print(" \\"); 
      break;
    }
      c++;
  }
  System.out.println("");  
  }//end of main method
}//end of class