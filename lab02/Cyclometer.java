/*Bradford DeMassa 
2 February, 2018
CSE2
The purpose of this program is to record different types of data of a 
bicycle trip. */
public class Cyclometer{
  public static void main (String[] args){
    int secsTrip1=480;             //Amount of time in seconds for Trip1
    int secsTrip2=3220;            //Amount o time in seconds for Trip2
    int countsTrip1=1561;          //Number of wheel rotations for Trip1
    int countsTrip2=9037;          //Number of wheel rotations for Trip2
    
    double WheelDiameter=27.0,                                //Wheel diameter in inchesPerFoot
    PI=3.14159,                                               //Identify assignment of PI
    feetPerMile=5280,                                         //Conversion of feet to mile
    inchesPerFoot=12,                                         //Converson of inches to feet
    secondsPerMinute=60;                                       //Conversion of seconds to minutes
    double distanceTrip1, distanceTrip2, totalDistance;       //Calculate distance of trips 1 & 2 
    
    System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts."); //Text display of the data stored for trip1
    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts."); //Text display of the data stored for trip2
    
    distanceTrip1=countsTrip1*WheelDiameter*PI/inchesPerFoot/feetPerMile;                              //Finds total distance in miles    
    distanceTrip2=countsTrip2*WheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;
    
    System.out.println("Trip 1 was "+ distanceTrip1 +" miles");               //Printing output data
    System.out.println("Trip 2 was "+ distanceTrip2 +" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
  }  //end of main method
}  //end of class
