/* Bradford DeMassa
CSE2
6 April 2018
This program is meant to ask for a 
number of student names, as well as
assign random grades to each one in 
an array format.*/
import java.util.Scanner;
public class Array{
  public static void main (String [] args){
    Scanner input = new Scanner (System.in);
    System.out.print("Enter the number of students: ");
    int numStudents = input.nextInt();
    String [] student;
    student = new String [numStudents];
    for (int i=0; i<numStudents;i++){
    System.out.print("Enter the students name: ");
    student [i] = input.next();
    }
    int [] grades;
    grades = new int [numStudents];
    for( int k=0; k<numStudents; k++){
      int l = (int) (Math.random()*100+1);
      grades [k] = l;
    }
    for (int j=0; j<numStudents;j++){
      System.out.println(student[j]+" : "+grades[j]);
    }
  }
}