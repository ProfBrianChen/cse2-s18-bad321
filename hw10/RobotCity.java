/*Bradford DeMassa
CSE2
23 April 2018
This program is supposed to be a 
representation of a city block in 
which robots invade and move around.
The city will eventually be occupied, as 
displayed by negative numbers.*/                //credentials and summary
public class RobotCity{
  public static void main (String[] args){
    int [][] cityArray = buildCity();
    display(cityArray,0);
    int k = (int) (Math.random()*25+1);
      System.out.println("");
      System.out.println("Number of invading robots: "+ k);
    invade(cityArray, k);
    display(cityArray,1);
    for (int l = 2; l<=6;l++){
    update(cityArray);
    display(cityArray,l);
    }
  }//end of main method
  public static int [][] buildCity(){
    int a = (int) (Math.random()*5+10);
    int b = (int) (Math.random()*5+10);
    int [][] cityArray = new int [a][b];
    for (int i =0; i<cityArray.length;i++){
      for (int j =0; j<cityArray[i].length;j++){
        cityArray[i][j] = (int) (Math.random()*899+100);
      }
    }
    return cityArray;
  }//end of buildCity method
  public static void display (int [][] cityArray, int iterations){
    System.out.println("CITY BLOCK POPULATIONS");
    if (iterations ==0){
      System.out.println("INITIAL BLOCK POPULATIONS BEFORE INVASION");
    }
    else {
      System.out.println("BLOCK POPULATIONS (DAY " + iterations + ")");
    }
    System.out.println("");
    for (int i =0; i<cityArray.length;i++){
      for (int j =0; j<cityArray[i].length;j++){
        System.out.printf(" %4d ",cityArray[i][j]);
      }
      System.out.println("");
      System.out.println("");
    }
  }//end of display method
  public static int [][] invade(int [][] cityArray, int k){
    for (int i=0;i<k;i++){
      int l = (int) (Math.random()* cityArray.length);
      int m = (int) (Math.random()* cityArray[l].length);
      if (cityArray[l][m]<0){
        i--;
        continue;
      }
      cityArray [l][m] = -1 * (cityArray[l][m]);
    }
    return cityArray;
  }//end of invade method
  public static int [][] update (int[][] cityArray){
    for (int i =0;i<cityArray.length;i++){
      for (int j=0; j<cityArray[i].length;j++){
        if (cityArray[i][j]>0){
          continue;
        }
        if (cityArray[i][j]<0 && j<cityArray[i].length-1){
          if(cityArray[i][j+1]>0){
          cityArray[i][j+1] = -1* (cityArray[i][j+1]);
            break;
          }
          continue;
      }
        continue;
    }
    }
    return cityArray;
  }//end of update method
}//end of class 