/* Bradford DeMassa
CSE2
7 April 2018
This program is meant to take in data and
search for a test grade as well as scramble
the data.*/
import java.util.Scanner;
import java.util.Random;
public class CSE2Linear{
  public static void main (String [] args){
    Scanner input = new Scanner (System.in);
    int grades [];
    grades = new int [15];
    for (int i = 0; i<15; i++){
      System.out.print("Enter a students grades in ascending order: ");
      boolean x = input.hasNextInt();
      if (x==false){
        input.nextLine();
        System.out.println("Error 1: Integer not entered!");
        i--;
        continue;
      }
      int val = input.nextInt();
      if (val>100 || val<0){
        input.nextLine();
        System.out.println("Error 2: Valid integer not entered!");
        i--;
        continue;
      }
      if (i!=0){
      if (val < grades[i-1]){
        input.nextLine();
        System.out.println("Error 3: Value is smaller than previous one!");
        i--;
        continue;
      }
      }
      grades [i] = val;
    }
    for (int j=0;j<15;j++){
      if (j<14){
    System.out.print(grades [j]+" ");
      }
      if (j==14){
        System.out.println(grades [j]);
      }
  }
    System.out.print("Enter a grade to search for: ");
    while (!input.hasNextInt()){
      System.out.println("Error 4: Invalid search number!");
      System.out.print("Re-enter a valid search number: ");
    }
    int y = input.nextInt();
    binary_search (grades, y);
    scramble (grades);
    linear_search (grades, y);
  }//end main method
  public static void binary_search (int grades[],int search){
    int count = 0;
    int iterations =0;
    int initial =0;
    int end = 14;
    while(initial<= end){
      iterations ++;
      int center = initial+(end-initial)/2;
      if (grades[center] == search){
        System.out.println(search+" Was found in the list with "+ iterations +" iterations.");
        count++;
        break;
      }
      if (grades[center] > search){
        end = center -1;
      }
      else {
        initial = center +1;
      }
    }//end of check counter
    if (count==0){
    System.out.println(search+ " Was not found in the list with " + iterations+ " iterations.");
    }
  }//end of binary search
  public static void linear_search (int x[], int search){
    int iterations = 1;
    for (int i =0; i<15; i++){
      if (x[i] ==search){
        System.out.println(search+" Was found in the list with "+ iterations +" iterations.");
        return;
      }
      else{
        iterations++;
      }
    }//end of for loop
       System.out.println(search+ " Was not found in the list with " + iterations+ " iterations.");
  }//end of linear search
  public static void scramble (int x []){
        Random num = new Random();
        for (int i = x.length-1; i >0; i--) {
            int j = num.nextInt(i);//random number within range of i, the length of the array
            int temp = x[i];
            x[i] = x[j];
            x[j] = temp;
        }//end of shuffling
for (int j=0;j<15;j++){
      if (j<14){
    System.out.print(x [j]+" ");
      }
      if (j==14){
        System.out.println(x [j]);
      }
  }//end of print statements
  }//end of scramble
  
}//end of class