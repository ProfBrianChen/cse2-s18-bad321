/*Bradford DeMassa
CSE2
9 April 2018
This is a modification to a source code
given for homework. Additions are given
to methods to complete the incomplete
code.*/
import java.util.Scanner;
import java.util.Random;
public class RemoveElements{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
  int num[]=new int[10];
  int newArray1[];
  int newArray2[];
  int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
    System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
	public static int [] randomInput (){
		int [] arr = new int[9];
		for (int i =1; i<arr.length; i++){
			Random num = new Random();
			int j = num.nextInt(i);
			arr [i] = j;
		}
		return arr;
		
	}
	public static int [] delete (int list [], int pos){
		if (pos>list.length-1){
			pos = list.length-1;
		}
		if (pos<0){
			pos = 0;
		}
		int [] list_1 = new int[list.length-1];
		for(int i = 0; list_1.length>= i;i++){
			if(i != pos){
				list_1[i] = list[i];
			}
		}
		return list_1;
	}
	public static int [] remove (int list [], int target){
		int count = 0;
		for (int i =0; i<list.length; i++){
      if (list[i] == target){
				count++;
      }
		}
		int [] list_1 = new int[list.length-count];
		for(int j = list_1.length; j>0;j++){
			if(j!=target){
				list_1[j] = list[j];
			}
		}
		return list_1;
	}
}
