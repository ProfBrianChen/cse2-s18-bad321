/* Bradford DeMassa
CSE2
20 April 2018*/
import java.util.Scanner;
public class lab10{
  public static void main (String[] args){
    boolean format;
    Scanner input = new Scanner (System.in);
    System.out.print("Input matrix width: ");
    int width = input.nextInt();
    System.out.print("Input matrix height: ");
    int height = input.nextInt();
    System.out.print("Type yes or no if matrix is row major: ");
    String y = "yes";
    String n = "no";
    String ans = input.nextLine();
    if (ans.equals(y)){
     format = true;
    }
    if (ans.equals(n)){
     format = false;
    }
    int [][] array = increasingmatrix(width,height,format);
    System.out.println("");
    printmatrix (array, format);
  }//end of main method
  public static int [][] increasingmatrix (int width, int height, boolean format){
    int [][] matrix = new int [width][height];
    int val = 0;
    if (format = true){
     for (int i =0; i<matrix.length;i++){
      for (int j=0; j<matrix[i].length; j++){
        val++;
        matrix [i][j] = val;
      }
     }
    }
    if (format == false){
      for(int k = 0; k<matrix.length; k++){
        for (int l = 0; l<matrix[k].length; l++){
          val++;
          matrix [k][l] = val;
        }
      }
    }
    return matrix;
  }//end of increasing matrix
  public static void printmatrix (int[][] array, boolean format){
   if (format == true){
     for (int i = 0; i<array.length;i++){
       for (int j = 0; j<array[i].length; j++){
         System.out.print (array[i][j] +" ");
       }
       System.out.println("");
     }
   }  
   if (format == false){
     for (int k = 0; k<array.length;k++){
       for (int l = 0; l <array.length; l++){
         System.out.print(array[l][k]+ " ");
       }
       System.out.println(" ");
     }
   }
  if (array == null){
    System.out.println("The array was empty!");
  }
  }//end of print matrix
}//end of class