/* Bradford DeMassa
CSE2
5 April 2018
This program is supposed to
generate random sentences, from
random words.*/
import java.util.Random;
public class lab07{
  public static void main (String [] args){
  Random RandomGenerator = new Random ();
  String c = Thesis();
  Action(c);
  Conclusion(c);
  }
  public static String adjectives (int x){
    switch (x){
      case 0:
        return " agreeable";
      case 1:
        return " brave";
      case 2:
        return " calm";
      case 3:
        return " delightful";
      case 4:
        return " eager";
      case 5:
        return " faithful";
      case 6:
        return " gentle";
      case 7:
        return " happy";
      case 8:
        return " new";
      case 9:
        return " strong";     
    }
    return "lol";
  }
  public static String npn_subject (int x){
    switch (x){
      case 0:
        return " cat";
      case 1:
        return " dog";
      case 2:
        return " fox";
      case 3:
        return " man";
      case 4:
        return " woman";
      case 5:
        return " bird";
      case 6:
        return " lizard";
      case 7:
        return " goldfish";
      case 8:
        return " sheep";
      case 9:
        return " flower";     
    }
    return "lol";
  }
  public static String pastVerbs (int x){
    switch (x){
      case 0:
        return " went";
      case 1:
        return " got";
      case 2:
        return " made";
      case 3:
        return " attacked";
      case 4:
        return " fought";
      case 5:
        return " thought";
      case 6:
        return " left";
      case 7:
        return " showed";
      case 8:
        return " heard";
      case 9:
        return " moved";     
    }
    return "lol";
  }
  public static String npn_object (int x){
    switch (x){
      case 0:
        return " mountain";
      case 1:
        return " sea";
      case 2:
        return " lake";
      case 3:
        return " forest";
      case 4:
        return " jungle";
      case 5:
        return " wolf";
      case 6:
        return " bear";
      case 7:
        return " turtle";
      case 8:
        return " building";
      case 9:
        return " man";     
    }
    return "lol";
  }
  public static String Action (String c){
    Random RandomGenerator = new Random ();
    int v = RandomGenerator.nextInt(10);
    for (int a1=0; a1<=v; a1++){
    int count = RandomGenerator.nextInt(2);
    int RandomInt = RandomGenerator.nextInt(10);
    String g = adjectives(RandomInt);
    int RandomInt_2 = RandomGenerator.nextInt(10);
    String h = adjectives(RandomInt_2);
    int RandomInt_3 = RandomGenerator.nextInt(10);
    String i = npn_subject(RandomInt_3);
    int RandomInt_4 = RandomGenerator.nextInt(10);
    String j = pastVerbs(RandomInt_4);
    int RandomInt_5 = RandomGenerator.nextInt(10);
    String k = npn_object(RandomInt_5);
    int RandomInt_6 = RandomGenerator.nextInt(10);
    String l = npn_object(RandomInt_6);
    if (count==0){
    System.out.println("This"+c+g+j+" the"+h+l+".");
    }
    else{
    System.out.println("It used the"+i+" to"+j+l+" at the"+h+k+".");
    }
    }
    return c;
  }
  public static String Thesis (){
    Random RandomGenerator = new Random ();
    int RandomInt = RandomGenerator.nextInt(10);
    String a = adjectives(RandomInt);
    int RandomInt_2 = RandomGenerator.nextInt(10);
    String b = adjectives(RandomInt_2);
    int RandomInt_3 = RandomGenerator.nextInt(10);  
    String c = npn_subject(RandomInt_3);
    int RandomInt_4 = RandomGenerator.nextInt(10);
    String d = pastVerbs(RandomInt_4);
    int RandomInt_5 = RandomGenerator.nextInt(10);
    String e = adjectives(RandomInt_5);
    int RandomInt_6 = RandomGenerator.nextInt(10);
    String f = npn_object(RandomInt_6);
    System.out.println("The"+a+b+c+d+" the"+e+f+".");
    return c;
  }
  public static String Conclusion (String c){
    Random RandomGenerator = new Random ();
    int RandomInt_4 = RandomGenerator.nextInt(10);
    String r = pastVerbs(RandomInt_4);
    int RandomInt_5 = RandomGenerator.nextInt(10);
    String s = npn_object(RandomInt_5);
    System.out.println("That"+c+r+" her"+s+".");
    return c;
  }
}