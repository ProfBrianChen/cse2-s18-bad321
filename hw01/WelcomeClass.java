//////////////////////////////////////////////
///CSE2 Welcome Class
///
public class WelcomeClass {
  public static void main (String[] args) {
    //prints Welcome to the terminal window
    System.out.println("   -------------       ");
    System.out.println("   |  Welcome  |       ");
    System.out.println("   -------------       ");
    //Prints the decoration symbol around the username in the terminal window
    System.out.println("  ^  ^  ^  ^  ^  ^  ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-B--A--D--3--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("  v  v  v  v  v  v  ");
    //Space from the decoration in the terminal window
    System.out.println(" ");
    //Bio paragraph printed to the terminal window
    System.out.println("Bio:");
    System.out.println("My name is Bradford DeMassa, I am a currently a freshman. I am a member of the");
    System.out.println("Lehigh XC/TF team, I also competed in high school at the national level. I am");
    System.out.println("from Ridgefield, Connecticut, where my father is a chiropractor, and my mother");
    System.out.println("is a nurse practitioner. I have two younger sisters in high school, one a junior,");
    System.out.println("one a freshman. I like to watch Star Wars and scary movies.");
  }  
}