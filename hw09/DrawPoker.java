/* Bradford DeMassa
CSE2
16 April 2018
This program is meant to simulate a 
poker game, in which a deck is shuffled
hands of five cards are drawn and a winner
is determined from high cards, pair, three
of a kind, full house, or flush. */
import java.util.Scanner;
public class DrawPoker{
  public static void main (String [] args){
    System.out.println("==================================");
    System.out.println("    Welcome to 2 Player Poker!    ");
    System.out.println("==================================");
    Scanner user = new Scanner (System.in);
    boolean x = true;
    String Y = "Yes";
    String N = "No";
    do{
      System.out.println("");
      System.out.print("Would you like to draw a hand? (Yes/No): ");
      String ans = user.nextLine();
      if (ans.equals(Y)){
        x = true;
        int win_count_p = 0;
        int win_count_d = 0;
        int [] dealer = shuffle();
        int [] player = shuffle();
        boolean a = pair (dealer);
        boolean b = pair (player);
        boolean c = three (dealer);
        boolean d = three (player);
        boolean e = full_house (dealer);
        boolean f = full_house (player);
        boolean g = flush (dealer);
        boolean h = flush (player);
        String [] dealer_f = printout (dealer);
        String [] player_f = printout (player);
        System.out.println("");
        System.out.println("---------------------------");
        System.out.println("     Dealer's Cards:       ");
        System.out.println("---------------------------");
        for (int i = 0; i<dealer.length;i++){
          System.out.println(dealer_f[i]);
        }
        System.out.println("");
        if (a == true && c == false){
          win_count_d = 2;
          System.out.println ("Pair!");
        }//pair
        if (c==true && e == false){
          win_count_d =3;
          System.out.println ("Three of a kind!");  
        }//three of a kind
        if (e==true && g == false){
          win_count_d = 4;
          System.out.println("Full House!");
        }//full house
        if (g == true){
          win_count_d = 5;
          System.out.println("Flush!");
        }//flush
        if (a == false && c == false && e == false && g == false){
          win_count_d = 1;
          System.out.println("High Card!");
        }//high card
        System.out.println("---------------------------");
        System.out.println("      Player's Cards:      ");
        System.out.println("---------------------------");
        for (int j =0; j<player.length; j++){
          System.out.println(player_f[j]);
        }
        System.out.println("");
        if (b == true && d == false){
          win_count_p =2;
          System.out.println("Pair!");
        }//pair
        if (d==true && f == false){
          win_count_p=3;
          System.out.println ("Three of a kind!");  
        } //three of a kind 
        if (f==true && h == false){
          win_count_p=4;
          System.out.println("Full House!");
        }//full house
        if (h==true){
          win_count_p =5;
          System.out.println("Flush!");
        }//flush
        if (b== false && d == false && f ==false && h==false){
          win_count_p = 1;
          System.out.println("High Card!");
        }//high card
        System.out.println("");
        System.out.println("===========================");
        System.out.println("          Winner           ");
        System.out.println("===========================");
        if (win_count_p<win_count_d){
          System.out.println("     The Dealer Wins!    ");
        }//dealer wins
        if (win_count_p>win_count_d){
          System.out.println("         You Win!        ");
        }//player wins
        if (win_count_p == win_count_d){
          System.out.println("          Draw!          ");
        }//draw
        System.out.println("");
        continue;
      }
      if (ans.equals(N)){
        x = false;
        break;
      }
      else {
        System.out.println("Error 1: Invalid answer input.");
      }
    } while (x = true);
        System.out.println("");
        System.out.println("===========================");
        System.out.println("         Goodbye!          ");
        System.out.println("===========================");   
  }//end of main method
  public static int [] shuffle (){
    int [] deck = new int [52];
    for (int i =0; i<deck.length; i++){
      deck [i] = i+1;
    }
    int [] hand = new int [5];
    for (int j = 0; j<hand.length;j++){
      int k = (int) (Math.random()*51);
      hand [j] = deck[k];
    }
    java.util.Arrays.sort (hand);
    for ( int l =0; l<hand.length-1; l++){
      if(hand [l] == hand [l+1]){
        hand [l+1] = hand [l] + 1;
      }
    }
    return hand;
  }//end of shuffle
  public static boolean pair (int [] hand){
    for (int j =0; j<hand.length; j++){
     for (int i =j+1; i<hand.length; i++){
      if (hand[i]%13 == hand[j]%13){
      return true;
        }
     }
    }
    return false;
  }//end of pair
  public static boolean three (int [] hand){
    for (int k = 0; k<hand.length; k++){
      for (int j =k+1; j<hand.length; j++){
        for (int i =j+1; i<hand.length; i++){
          if (hand[i]%13 == hand[j]%13 && hand [j]%13 == hand[k]%13){
            return true;
          }
        }
      }
    }
    return false;
  }//end of three of a kind 
  public static boolean full_house (int[] hand){
    for (int i =0; i<hand.length-2; i++){
     if (hand[i]%13 == hand[i+1]%13 && hand[i+1]%13 == hand [i+2]%13){
       if (hand [hand.length-i-1]%13 == hand[hand.length-i-2]%13){
        return true;
       }
     }
    }
    return false;    
  }//end of full house
  public static boolean flush (int[] hand){
    if (hand[0]/13 == hand [1]/13 && hand [1]/13 == hand[2]/13 && hand[2]/13 == hand[3]/13 && hand[3]/13 == hand[4]/13){
      return true;
    }
    return false;
  }// end of flush
  public static String [] printout (int[] hand){
   String [] suit_o = new String [hand.length];
   String [] suit = new String [hand.length];
   String [] print = new String [hand.length];
   for (int j = 0; j<hand.length; j++){
     switch (hand [j]/13){
       case 0:
         suit_o[j] = "Diamonds";
         break;
       case 1:
         suit_o[j] = "Clubs";
         break;
       case 2:
         suit_o[j] = "Hearts";
         break;
       case 3:
         suit_o[j] = "Spades";
         break;
     }
   }
   for (int i = 0; i<hand.length; i++){
    switch (hand [i]%13){
      case 0: 
        suit [i] = "Ace";
        break;
      case 1: 
        suit [i] = "2";
        break;
      case 2: 
        suit [i] = "3";
        break;
      case 3: 
        suit [i] = "4";
        break;
      case 4: 
        suit [i] = "5";
        break;
      case 5: 
        suit [i] = "6";
        break;
      case 6: 
        suit [i] = "7";
        break;
      case 7: 
        suit [i] = "8";
        break;
      case 8: 
        suit [i] = "9";
        break;
      case 9: 
        suit [i] = "10";
        break;
      case 10: 
        suit [i] = "Jack";
        break;
      case 11: 
        suit [i] = "Queen";
        break;
      case 12: 
        suit [i] = "King";
        break;     
    }//end of switch
  }
   for (int k = 0; k<hand.length; k++){
     print [k] = suit[k] +" of "+ suit_o[k];
   }
    return print;
  }//end of printout
}//end of class



