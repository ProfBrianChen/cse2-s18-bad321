/*Bradford DeMassa
CSE 2
18 February 2018
This program is meant to play Yahtzee, and give a random roll, or use numbers based on a users input. */
import java.util.Random;
import java.util.Scanner;
public class Yahtzee {
  public static void main (String [] args){
    
    int upperscore = 0;
    int lowerscore = 0;
    int a=1;
    int box_1=0;
    int box_2=0;
    int box_3=0;
    int box_4=0;
    int box_5=0;
    int box_6=0;
    int box_7=0;
    int box_8=0;
    int box_9=0;
    int box_10=0;
    int box_11=0;
    int box_12=0;
    int box_13=0;
    int box_count = box_1 + box_2 + box_3 + box_4 + box_5 + box_6 + box_7 + box_8 + box_9 + box_10 + box_11 + box_12 + box_13;
        Scanner question = new Scanner (System.in); //Constructs the scanner to recive input
    while (a<=13){
      int die_1, die_2, die_3, die_4, die_5;
     System.out.print("Would you like to enter numbers for a roll? (Yes/No) ");
      String answer = question.next ();
      String answer_w = "Yes";
      String answer_x = "No";
       if (answer.equals(answer_w)){
        System.out.print("Enter a five digit number: ");
        int input = question.nextInt();
        die_5 = input % 10;
        die_4 = ((input % 100) - die_5)/10;
        die_3 = ((input % 1000) - (input % 100))/100;
        die_2 = ((input % 10000) - (input % 1000))/1000;
        die_1 = ((input % 100000) - (input % 10000))/10000;
         if (die_1>6 || die_1<1 || die_2>6 || die_1<1 || die_3>6 || die_3<1 || die_4>6 || die_4<1 || die_5>6 || die_5<1){
           System.out.println("Error, the digits must be between 1 and 6.");
           a--;
         }//if input is above a 6 or less than 1
         else if (input>66666 || input<11111){
           System.out.println("The number must contain EXACTLY 5 digits.");
           a--;
         }//if more than 5 digits, or if input is less than 5 digits
         else {     
            System.out.println("Roll number: " + a);
            System.out.println("Die 1: " + die_1);
            System.out.println("Die 2: " + die_2);
            System.out.println("Die 3: " + die_3);
            System.out.println("Die 4: " + die_4);
            System.out.println("Die 5: " + die_5);         
                 
              int counter_1 = 0; //How many of each type of die there is 
              int counter_2 = 0;
              int counter_3 = 0;
              int counter_4 = 0;
              int counter_5 = 0;
              int counter_6 = 0;    
              switch (die_1){
      case 1:
        ++counter_1;
        break;
      case 2:
        ++counter_2;
        break;
      case 3:
        ++counter_3;
        break;
      case 4:
        ++counter_4;
        break;
      case 5:
        ++counter_5;
        break;
      case 6:
        ++counter_6;
        break;
    } //adds values to the counters to determine how many of each number there is  
              switch (die_2){
      case 1:
        ++counter_1;
        break;
      case 2:
        ++counter_2;
        break;
      case 3:
        ++counter_3;
        break;
      case 4:
        ++counter_4;
        break;
      case 5:
        ++counter_5;
        break;
      case 6:
        ++counter_6;
        break;
    }  
              switch (die_3){
      case 1:
        ++counter_1;
        break;
      case 2:
        ++counter_2;
        break;
      case 3:
        ++counter_3;
        break;
      case 4:
        ++counter_4;
        break;
      case 5:
        ++counter_5;
        break;
      case 6:
        ++counter_6;
        break;
    }   
              switch (die_4){
      case 1:
        ++counter_1;
        break;
      case 2:
        ++counter_2;
        break;
      case 3:
        ++counter_3;
        break;
      case 4:
        ++counter_4;
        break;
      case 5:
        ++counter_5;
        break;
      case 6:
        ++counter_6;
        break;
    }   
              switch (die_5){
      case 1:
        ++counter_1;
        break;
      case 2:
        ++counter_2;
        break;
      case 3:
        ++counter_3;
        break;
      case 4:
        ++counter_4;
        break;
      case 5:
        ++counter_5;
        break;
      case 6:
        ++counter_6;
        break;
    }          
              if(box_1<1){
                if (box_count==0){
                  box_1++;
                  System.out.println("Box 1 has been filled");
                }
              if (counter_1>2){
                upperscore = upperscore + counter_1;
                System.out.println("Aces!");
                System.out.println("Box 1 has been filled");
              }
              }//Aces?
              if(box_2<1){
                if (box_count==1){
                  box_2++;
                  System.out.println("Box 2 has been filled");
                }
              if (counter_2>2){
                upperscore = upperscore + 2 * counter_2;
                System.out.println("Twos!");
                System.out.println("Box 2 has been filled");
              }
              }//Twos?
              if(box_3<1){
                if (box_count==2){
                  box_3++;
                  System.out.println("Box 3 has been filled");
                }
              if (counter_3>3){
                upperscore = upperscore + 3 * counter_3;
                System.out.println("Threes!");
                System.out.println("Box 3 has been filled");
              }
              }//Threes?
              if(box_4<1){
                 if (box_count==3){
                  box_4++;
                  System.out.println("Box 4 has been filled");
                }
              if (counter_4>2){
                upperscore = upperscore + 4 * counter_4;
                System.out.println("Fours!");
                System.out.println("Box 4 has been filled");
              }
              }//Fours?
              if(box_5<1){
                if (box_count==4){
                  box_5++;
                  System.out.println("Box 5 has been filled");
                }
              if (counter_5>2){
                upperscore = upperscore + 5 * counter_5;
                System.out.println("Fives!");
                System.out.println("Box 5 has been filled");
              }
              }//Fives?
              if(box_6<1){
                 if (box_count==5){
                  box_6++;
                  System.out.println("Box 6 has been filled");
                }
              if (counter_6>2){
                upperscore = upperscore + 6 * counter_6;
                System.out.println("Sixes!");
                System.out.println("Box 6 has been filled");
              }
              }//Sixes?        
              if(box_7<1 || box_8<1){
              switch (counter_1){
         case 3:
           if(box_7<1){
           box_7++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Three of a kind!");
           System.out.println("Box 7 has been filled");
           }
           if (box_count == 6){
                    box_7++;
                    System.out.println("Box 7 has been filled");
                  }
           break;
         case 4:
           if(box_8<1){
           box_8++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Four of a kind!");
           System.out.println("Box 8 has been filled");
           }
           if (box_count == 7){
                    box_8++;
                    System.out.println("Box 8 has been filled");
                  }   
           break;
       }//Three and four of a kind
              switch (counter_2){
         case 3:
           if(box_7<1){
           box_7++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Three of a kind!");
           System.out.println("Box 7 has been filled");
           }
           if (box_count == 6){
                    box_7++;
                    System.out.println("Box 7 has been filled");
                  }
           break;
         case 4:
           if(box_8<1){
           box_8++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Four of a kind!");
           System.out.println("Box 8 has been filled");
           }
           if (box_count == 7){
                    box_8++;
                    System.out.println("Box 8 has been filled");
                  }   
           break;
       }//Three and four of a kind
              switch (counter_3){
         case 3:
           if(box_7<1){
           box_7++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Three of a kind!");
           System.out.println("Box 7 has been filled");
           }
           if (box_count == 6){
                    box_7++;
                    System.out.println("Box 7 has been filled");
                  }
           break;
         case 4:
           if(box_8<1){
           box_8++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Four of a kind!");
           System.out.println("Box 8 has been filled");
           }
           if (box_count == 7){
                    box_8++;
                    System.out.println("Box 8 has been filled");
                  }   
           break;
       }//Three and four of a kind
              switch (counter_4){
         case 3:
           if(box_7<1){
           box_7++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Three of a kind!");
           System.out.println("Box 7 has been filled");
           }
           if (box_count == 6){
                    box_7++;
                    System.out.println("Box 7 has been filled");
                  }
           break;
         case 4:
           if(box_8<1){
           box_8++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Four of a kind!");
           System.out.println("Box 8 has been filled");
           }
           if (box_count == 7){
                    box_8++;
                    System.out.println("Box 8 has been filled");
                  }   
           break;
       }//Three and four of a kind
              switch (counter_5){
         case 3:
           if(box_7<1){
           box_7++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Three of a kind!");
           System.out.println("Box 7 has been filled");
           }
           if (box_count == 6){
                    box_7++;
                    System.out.println("Box 7 has been filled");
                  }
           break;
         case 4:
           if(box_8<1){
           box_8++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Four of a kind!");
           System.out.println("Box 8 has been filled");
           }
           if (box_count == 7){
                    box_8++;
                    System.out.println("Box 8 has been filled");
                  }   
           break;
       }//Three and four of a kind
              switch (counter_6){
         case 3:
           if(box_7<1){
           box_7++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Three of a kind!");
           System.out.println("Box 7 has been filled");
           }
           if (box_count == 6){
                    box_7++;
                    System.out.println("Box 7 has been filled");
                  }
           break;
         case 4:
           if(box_8<1){
           box_8++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Four of a kind!");
           System.out.println("Box 8 has been filled");
           }
           if (box_count == 7){
                    box_8++;
                    System.out.println("Box 8 has been filled");
                  }   
           break;
       }//Three and four of a kind
              }//3 or 4 of a kind?
              if(box_9<1){
                if (counter_1==2){
                if (counter_2==3 || counter_3==3 || counter_4==3 || counter_5==3 || counter_6==3){
                  box_9++;
                  lowerscore = lowerscore +25;
                  System.out.println("Full House!");
                  System.out.println("Box 9 has been filled");
                  }
                }//full house
                if (counter_2==2){
                if (counter_1==3 || counter_3==3 || counter_4==3 || counter_5==3 || counter_6==3){
                 box_9++;
                 lowerscore = lowerscore +25;
                 System.out.println("Full House!");
                 System.out.println("Box 9 has been filled");
                  }
              }
                if (counter_3==2){
                if (counter_2==3 || counter_1==3 || counter_4==3 || counter_5==3 || counter_6==3){
                box_9++;
                lowerscore = lowerscore +25;
                System.out.println("Full House!");
                System.out.println("Box 9 has been filled");
                }
               }
                if (counter_4==2){
                  if (counter_2==3 || counter_3==3 || counter_1==3 || counter_5==3 || counter_6==3){
                    box_9++;
                    lowerscore = lowerscore +25;
                    System.out.println("Full House!");
                    System.out.println("Box 9 has been filled");
                    }
                  }
                if (counter_5==2){
                  if (counter_2==3 || counter_3==3 || counter_4==3 || counter_1==3 || counter_6==3){
                    box_9++;
                    lowerscore = lowerscore +25;
                    System.out.println("Full House!");
                    System.out.println("Box 9 has been filled");
                   }
                }
                if (counter_6==2){
                if (counter_2==3 || counter_3==3 || counter_4==3 || counter_5==3 || counter_1==3){
                   box_9++;
                   lowerscore = lowerscore +25;
                   System.out.println("Full House!");
                   System.out.println("Box 9 has been filled");
                    }
                  }
                if (box_count==8){
                  box_9++;
                  System.out.println("Box 9 has been filled");
                }//autofills box 5th to last
              }//full house?
              if(box_10<1){
                if(die_1 == 1 || die_2 == 1 || die_3 == 1 || die_4 == 1 || die_5 == 1){
                  if(die_1 == 2 || die_2 == 2 || die_3 == 2 || die_4 == 2 || die_5 == 2){
                    if(die_1 == 3 || die_2 == 3 || die_3 == 3 || die_4 == 3 || die_5 == 3){
                      if(die_1 == 4 || die_2 == 4 || die_3 == 4 || die_4 == 4 || die_5 == 4){
                        box_10++;
                        lowerscore = lowerscore + 30;
                        System.out.println("Small Straight!");
                        System.out.println("Box 10 has been filled");
                      }
                    }  
                  }
                }
                if(die_1 == 2 || die_2 == 2 || die_3 == 2 || die_4 == 2 || die_5 == 2){
                  if(die_1 == 3 || die_2 == 3 || die_3 == 3 || die_4 == 3 || die_5 == 3){
                    if(die_1 == 4 || die_2 == 4 || die_3 == 4 || die_4 == 4 || die_5 == 4){
                      if(die_1 == 5 || die_2 == 5 || die_3 == 5 || die_4 == 5 || die_5 == 5){
                        box_10++;
                        lowerscore = lowerscore + 30;
                        System.out.println("Small Straight!");
                        System.out.println("Box 10 has been filled");
                      }
                    }  
                  }
                }
                if(die_1 == 3 || die_2 == 3 || die_3 == 3 || die_4 == 3 || die_5 == 3){
                  if(die_1 == 4 || die_2 == 4 || die_3 == 4 || die_4 == 4 || die_5 == 4){
                    if(die_1 == 5 || die_2 == 5 || die_3 == 5 || die_4 == 5 || die_5 == 5){
                      if(die_1 == 6 || die_2 == 6 || die_3 == 6 || die_4 == 6 || die_5 == 6){
                        box_10++;
                        lowerscore = lowerscore + 30;
                        System.out.println("Small Straight!");
                        System.out.println("Box 10 has been filled");
                      }
                    }  
                  }
                }
                if(box_count == 9){
                  box_10++;
                  System.out.println("Box 10 has been filled");
                }
              }//small straight?
              if(box_11<1){
                if (counter_1<2 && counter_2<2 && counter_3<2 && counter_4<2 && counter_5<2 && counter_6<2){//no counter will be above 2 in a large straight
                  if (counter_1 == 0 || counter_6 == 0){//counter one or six has to be zero for a large straight to be true
                    box_11++;
                    lowerscore = lowerscore + 40;
                    System.out.println("Large Straight!");
                    System.out.println("Box 11 has been filled");
                 }
                }
                if (box_count==10){
                  box_11++;
                  System.out.println("Box 11 has been filled");
                }//autofills box 3rd to last
              }//large straight?
              if(box_12<1){
                if(box_count==11){
                  int chance = die_1 + die_2 + die_3 + die_4 + die_5;//chance
                  box_12++;
                  System.out.print("Chance!");
                  System.out.print("Box 12 has been filled");
                }//autofills chance box on roll 12
              }//chance?
              if(box_13<1){
                if (die_1==die_2 && die_2==die_3 && die_3==die_4 && die_4==die_5){
                box_13++;
                lowerscore = lowerscore +50;
                System.out.println("YAHTZEE!!!");
                System.out.println("Box 13 has been filled");
                }//yahtzee
                if (box_count == 12){
                box_13++;
                System.out.println("Box 13 has been filled");
                }//autofills box last
              }//yahtzee?         
         }
    }
       if (answer.equals(answer_x)){
            die_1 = (int) (Math.random()*6)+1;//first dice roll
            die_2 = (int) (Math.random()*6)+1;//second dice roll
            die_3 = (int) (Math.random()*6)+1;//third dice roll
            die_4 = (int) (Math.random()*6)+1;//fourth dice roll
            die_5 = (int) (Math.random()*6)+1;//fifth dice roll
             System.out.println("Roll number: " + a); 
             System.out.println("Die 1: " + die_1);
             System.out.println("Die 2: " + die_2);
             System.out.println("Die 3: " + die_3);
             System.out.println("Die 4: " + die_4);
             System.out.println("Die 5: " + die_5);      
                 
              int counter_1 = 0; //How many of each type of die there is 
              int counter_2 = 0;
              int counter_3 = 0;
              int counter_4 = 0;
              int counter_5 = 0;
              int counter_6 = 0;    
              switch (die_1){
      case 1:
        ++counter_1;
        break;
      case 2:
        ++counter_2;
        break;
      case 3:
        ++counter_3;
        break;
      case 4:
        ++counter_4;
        break;
      case 5:
        ++counter_5;
        break;
      case 6:
        ++counter_6;
        break;
    } //adds values to the counters to determine how many of each number there is  
              switch (die_2){
      case 1:
        ++counter_1;
        break;
      case 2:
        ++counter_2;
        break;
      case 3:
        ++counter_3;
        break;
      case 4:
        ++counter_4;
        break;
      case 5:
        ++counter_5;
        break;
      case 6:
        ++counter_6;
        break;
    }  
              switch (die_3){
      case 1:
        ++counter_1;
        break;
      case 2:
        ++counter_2;
        break;
      case 3:
        ++counter_3;
        break;
      case 4:
        ++counter_4;
        break;
      case 5:
        ++counter_5;
        break;
      case 6:
        ++counter_6;
        break;
    }   
              switch (die_4){
      case 1:
        ++counter_1;
        break;
      case 2:
        ++counter_2;
        break;
      case 3:
        ++counter_3;
        break;
      case 4:
        ++counter_4;
        break;
      case 5:
        ++counter_5;
        break;
      case 6:
        ++counter_6;
        break;
    }   
              switch (die_5){
      case 1:
        ++counter_1;
        break;
      case 2:
        ++counter_2;
        break;
      case 3:
        ++counter_3;
        break;
      case 4:
        ++counter_4;
        break;
      case 5:
        ++counter_5;
        break;
      case 6:
        ++counter_6;
        break;
    }          
              if(box_1<1){
                if (box_count==0){
                  box_1++;
                  System.out.println("Box 1 has been filled");
                }
              if (counter_1>2){
                upperscore = upperscore + counter_1;
                System.out.println("Aces!");
                System.out.println("Box 1 has been filled");
              }
              }//Aces?
              if(box_2<1){
                if (box_count==1){
                  box_2++;
                  System.out.println("Box 2 has been filled");
                }
              if (counter_2>2){
                upperscore = upperscore + 2 * counter_2;
                System.out.println("Twos!");
                System.out.println("Box 2 has been filled");
              }
              }//Twos?
              if(box_3<1){
                if (box_count==2){
                  box_3++;
                  System.out.println("Box 3 has been filled");
                }
              if (counter_3>3){
                upperscore = upperscore + 3 * counter_3;
                System.out.println("Threes!");
                System.out.println("Box 3 has been filled");
              }
              }//Threes?
              if(box_4<1){
                 if (box_count==3){
                  box_4++;
                  System.out.println("Box 4 has been filled");
                }
              if (counter_4>2){
                upperscore = upperscore + 4 * counter_4;
                System.out.println("Fours!");
                System.out.println("Box 4 has been filled");
              }
              }//Fours?
              if(box_5<1){
                if (box_count==4){
                  box_5++;
                  System.out.println("Box 5 has been filled");
                }
              if (counter_5>2){
                upperscore = upperscore + 5 * counter_5;
                System.out.println("Fives!");
                System.out.println("Box 5 has been filled");
              }
              }//Fives?
              if(box_6<1){
                 if (box_count==5){
                  box_6++;
                  System.out.println("Box 6 has been filled");
                }
              if (counter_6>2){
                upperscore = upperscore + 6 * counter_6;
                System.out.println("Sixes!");
                System.out.println("Box 6 has been filled");
              }
              }//Sixes?        
              if(box_7<1 || box_8<1){
              switch (counter_1){
         case 3:
           if(box_7<1){
           box_7++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Three of a kind!");
           System.out.println("Box 7 has been filled");
           }
           if (box_count == 6){
                    box_7++;
                    System.out.println("Box 7 has been filled");
                  }
           break;
         case 4:
           if(box_8<1){
           box_8++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Four of a kind!");
           System.out.println("Box 8 has been filled");
           }
           if (box_count == 7){
                    box_8++;
                    System.out.println("Box 8 has been filled");
                  }   
           break;
       }//Three and four of a kind
              switch (counter_2){
         case 3:
           if(box_7<1){
           box_7++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Three of a kind!");
           System.out.println("Box 7 has been filled");
           }
           if (box_count == 6){
                    box_7++;
                    System.out.println("Box 7 has been filled");
                  }
           break;
         case 4:
           if(box_8<1){
           box_8++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Four of a kind!");
           System.out.println("Box 8 has been filled");
           }
           if (box_count == 7){
                    box_8++;
                    System.out.println("Box 8 has been filled");
                  }   
           break;
       }//Three and four of a kind
              switch (counter_3){
         case 3:
           if(box_7<1){
           box_7++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Three of a kind!");
           System.out.println("Box 7 has been filled");
           }
           if (box_count == 6){
                    box_7++;
                    System.out.println("Box 7 has been filled");
                  }
           break;
         case 4:
           if(box_8<1){
           box_8++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Four of a kind!");
           System.out.println("Box 8 has been filled");
           }
           if (box_count == 7){
                    box_8++;
                    System.out.println("Box 8 has been filled");
                  }   
           break;
       }//Three and four of a kind
              switch (counter_4){
         case 3:
           if(box_7<1){
           box_7++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Three of a kind!");
           System.out.println("Box 7 has been filled");
           }
           if (box_count == 6){
                    box_7++;
                    System.out.println("Box 7 has been filled");
                  }
           break;
         case 4:
           if(box_8<1){
           box_8++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Four of a kind!");
           System.out.println("Box 8 has been filled");
           }
           if (box_count == 7){
                    box_8++;
                    System.out.println("Box 8 has been filled");
                  }   
           break;
       }//Three and four of a kind
              switch (counter_5){
         case 3:
           if(box_7<1){
           box_7++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Three of a kind!");
           System.out.println("Box 7 has been filled");
           }
           if (box_count == 6){
                    box_7++;
                    System.out.println("Box 7 has been filled");
                  }
           break;
         case 4:
           if(box_8<1){
           box_8++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Four of a kind!");
           System.out.println("Box 8 has been filled");
           }
           if (box_count == 7){
                    box_8++;
                    System.out.println("Box 8 has been filled");
                  }   
           break;
       }//Three and four of a kind
              switch (counter_6){
         case 3:
           if(box_7<1){
           box_7++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Three of a kind!");
           System.out.println("Box 7 has been filled");
           }
           if (box_count == 6){
                    box_7++;
                    System.out.println("Box 7 has been filled");
                  }
           break;
         case 4:
           if(box_8<1){
           box_8++;
           lowerscore = lowerscore + die_1 + die_2 + die_3 + die_4 + die_5;
           System.out.println("Four of a kind!");
           System.out.println("Box 8 has been filled");
           }
           if (box_count == 7){
                    box_8++;
                    System.out.println("Box 8 has been filled");
                  }   
           break;
       }//Three and four of a kind
              }//3 or 4 of a kind?
              if(box_9<1){
                if (counter_1==2){
                if (counter_2==3 || counter_3==3 || counter_4==3 || counter_5==3 || counter_6==3){
                  box_9++;
                  lowerscore = lowerscore +25;
                  System.out.println("Full House!");
                  System.out.println("Box 9 has been filled");
                  }
                }//full house
                if (counter_2==2){
                if (counter_1==3 || counter_3==3 || counter_4==3 || counter_5==3 || counter_6==3){
                 box_9++;
                 lowerscore = lowerscore +25;
                 System.out.println("Full House!");
                 System.out.println("Box 9 has been filled");
                  }
              }
                if (counter_3==2){
                if (counter_2==3 || counter_1==3 || counter_4==3 || counter_5==3 || counter_6==3){
                box_9++;
                lowerscore = lowerscore +25;
                System.out.println("Full House!");
                System.out.println("Box 9 has been filled");
                }
               }
                if (counter_4==2){
                  if (counter_2==3 || counter_3==3 || counter_1==3 || counter_5==3 || counter_6==3){
                    box_9++;
                    lowerscore = lowerscore +25;
                    System.out.println("Full House!");
                    System.out.println("Box 9 has been filled");
                    }
                  }
                if (counter_5==2){
                  if (counter_2==3 || counter_3==3 || counter_4==3 || counter_1==3 || counter_6==3){
                    box_9++;
                    lowerscore = lowerscore +25;
                    System.out.println("Full House!");
                    System.out.println("Box 9 has been filled");
                   }
                }
                if (counter_6==2){
                if (counter_2==3 || counter_3==3 || counter_4==3 || counter_5==3 || counter_1==3){
                   box_9++;
                   lowerscore = lowerscore +25;
                   System.out.println("Full House!");
                   System.out.println("Box 9 has been filled");
                    }
                  }
                if (box_count==8){
                  box_9++;
                  System.out.println("Box 9 has been filled");
                }//autofills box 5th to last
              }//full house?
              if(box_10<1){
                if(die_1 == 1 || die_2 == 1 || die_3 == 1 || die_4 == 1 || die_5 == 1){
                  if(die_1 == 2 || die_2 == 2 || die_3 == 2 || die_4 == 2 || die_5 == 2){
                    if(die_1 == 3 || die_2 == 3 || die_3 == 3 || die_4 == 3 || die_5 == 3){
                      if(die_1 == 4 || die_2 == 4 || die_3 == 4 || die_4 == 4 || die_5 == 4){
                        box_10++;
                        lowerscore = lowerscore + 30;
                        System.out.println("Small Straight!");
                        System.out.println("Box 10 has been filled");
                      }
                    }  
                  }
                }
                if(die_1 == 2 || die_2 == 2 || die_3 == 2 || die_4 == 2 || die_5 == 2){
                  if(die_1 == 3 || die_2 == 3 || die_3 == 3 || die_4 == 3 || die_5 == 3){
                    if(die_1 == 4 || die_2 == 4 || die_3 == 4 || die_4 == 4 || die_5 == 4){
                      if(die_1 == 5 || die_2 == 5 || die_3 == 5 || die_4 == 5 || die_5 == 5){
                        box_10++;
                        lowerscore = lowerscore + 30;
                        System.out.println("Small Straight!");
                        System.out.println("Box 10 has been filled");
                      }
                    }  
                  }
                }
                if(die_1 == 3 || die_2 == 3 || die_3 == 3 || die_4 == 3 || die_5 == 3){
                  if(die_1 == 4 || die_2 == 4 || die_3 == 4 || die_4 == 4 || die_5 == 4){
                    if(die_1 == 5 || die_2 == 5 || die_3 == 5 || die_4 == 5 || die_5 == 5){
                      if(die_1 == 6 || die_2 == 6 || die_3 == 6 || die_4 == 6 || die_5 == 6){
                        box_10++;
                        lowerscore = lowerscore + 30;
                        System.out.println("Small Straight!");
                        System.out.println("Box 10 has been filled");
                      }
                    }  
                  }
                }
                if(box_count == 9){
                  box_10++;
                  System.out.println("Box 10 has been filled");
                }
              }//small straight?
              if(box_11<1){
                if (counter_1<2 && counter_2<2 && counter_3<2 && counter_4<2 && counter_5<2 && counter_6<2){//no counter will be above 2 in a large straight
                  if (counter_1 == 0 || counter_6 == 0){//counter one or six has to be zero for a large straight to be true
                    box_11++;
                    lowerscore = lowerscore + 40;
                    System.out.println("Large Straight!");
                    System.out.println("Box 11 has been filled");
                 }
                }
                if (box_count==10){
                  box_11++;
                  System.out.println("Box 11 has been filled");
                }//autofills box 3rd to last
              }//large straight?
              if(box_12<1){
                if(box_count==11){
                  int chance = die_1 + die_2 + die_3 + die_4 + die_5;//chance
                  box_12++;
                  System.out.print("Chance!");
                  System.out.print("Box 12 has been filled");
                }//autofills chance box on roll 12
              }//chance?
              if(box_13<1){
                if (die_1==die_2 && die_2==die_3 && die_3==die_4 && die_4==die_5){
                box_13++;
                lowerscore = lowerscore +50;
                System.out.println("YAHTZEE!!!");
                System.out.println("Box 13 has been filled");
                }//yahtzee
                if (box_count == 12){
                box_13++;
                System.out.println("Box 13 has been filled");
                }//autofills box last
              }//yahtzee?         
       }  
    a++;
  }
    if(upperscore>63){
      int upperscore_b = upperscore +35;
      int grandtotal = upperscore_b + lowerscore;
    System.out.println("The Upper score is :" + upperscore);
    System.out.println("The Upper score with bonus is :" + upperscore_b);
    System.out.println("The Lower score is :" + lowerscore);
    System.out.println("The Grand total is :" + grandtotal);
    }
    else{
    int grandtotal = upperscore + lowerscore;
    System.out.println("The Upper score is :" + upperscore);
    System.out.println("The Upper score with bonus is :" + upperscore);
    System.out.println("The Lower score is :" + lowerscore);
    System.out.println("The Grand total is :" + grandtotal);
    }
  }//end main method
}//end of class