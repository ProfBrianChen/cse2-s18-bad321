/* Bradford DeMassa
CSE2
13 April 2018
This program is meant to 
swap the order of arrays 
and print them out*/
public class lab09 {
  public static void main (String [] args){
    int [] array0 = {0,1,2,3,4,5,6,7,8,9}; 
    int [] array1 = copy (array0);
    int [] array2 = copy (array0);
    inverter (array0);
    System.out.print("Array 0:");
    print (array0);
    inverter_2 (array1);
    System.out.print("Array 1:");
    print (array1);
    int [] array3 = inverter_2 (array2);
    System.out.print("Array 3:");
    print (array3);
  }//end main method
  public static int[] copy (int original []){
    int [] copy = new int [original.length];
    for (int i=0; i<original.length;i++){
      copy[i] = original [i];
    }
    return copy;
  }//end of copy method
  public static void inverter (int original []){
    int [] temp = new int [original.length]; 
    for (int i=0; i<original.length/2; i++){
      temp [i] = original [i];
      original [i] = original[original.length-1-i];
      original[original.length-1-i] = temp[i];
    }
  }//end of inverter
  public static int[] inverter_2 (int original[]){
    int [] copy = copy(original);
    inverter (copy);
    return copy;
  }//end of inverter 2
  public static void print (int original []){
    System.out.print(" { ");
   for (int i = 0; i<original.length; i++){
     if (i!= original.length-1){
     System.out.print(original[i]+ ", ");
     }
     if (i== original.length-1){
       System.out.print(original[i] + " }");
     }
   }
    System.out.println("");
}//end of print
}//end of class