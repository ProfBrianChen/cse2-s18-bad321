/*Bradford DeMassa
CSE2 
16 February 2018
This program is meant to be a random card picker. */
import java.util.Random;
public class CardGenerator {
  public static void main (String [] args) {
    int cardNum = (int) (Math.random()*52)+1;
    String suit = "a";
    String identity = "b";
    if (cardNum >=1 && cardNum <=13) {      //if statements identifies suits
        suit = "Diamonds";
      }
      if (cardNum >=14 && cardNum <=26) {
        suit = "Clubs";//Club cards
      }
      if (cardNum >=27 && cardNum <=39) { 
        suit = "Hearts";//Hearts cards
      }
      if (cardNum >=40 && cardNum <=52) {
        suit = "Spades";//Spades cards
      }
      int rem = (int) cardNum % 13;//specializes card types through remainder math
      switch (rem){//switch statement identifies card types
        case 1:
          identity = "Ace";
          break;
        case 2:
          identity = "2";
          break;
        case 3:
          identity = "3";
          break;
        case 4:
          identity = "4";
          break;
        case 5:
          identity = "5";
          break;
        case 6:
          identity = "6";
          break;
        case 7:
          identity = "7";
          break;
        case 8:
          identity = "8";
          break;
        case 9:
          identity = "9";
          break;
        case 10:
          identity = "10";
          break;
        case 11:
          identity = "Jack";
          break;
        case 12:
          identity = "Queen";
          break;
        case 13:
          identity = "King";
          break; 
      }
    System.out.println("You picked " + identity + " of " + suit);//prints what card was picked
    System.out.println("The random number generated was " + cardNum);//gives random number
  }//end of main method
}//end of class