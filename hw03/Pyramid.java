/*Bradford DeMassa
CSE2
12 February 2018
This program is supposed to prompt the user for dimensions of a pyramid and return the area of said pyramid*/
import java.util.Scanner;
public class Pyramid{
public static void main (String[] args){
    Scanner myScanner = new Scanner (System.in);
    System.out.print("The square side of the pyramid is (input length):");
    double a = myScanner.nextDouble ();
    System.out.print("The height of the pyramid is (input height):");
    double h = myScanner.nextDouble ();
    
    double a2 = Math.pow(a, 2);
    
    double aTot= a2 * (h/3);
    
    System.out.println("The volume of the pyramid is:" + aTot);
    
  }//End of main method
}//End of class
