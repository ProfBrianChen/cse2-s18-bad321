/*Bradford DeMassa
CSE2
12 February 2018
This program is meant to convert quantities of rain from a hurricane into cubic miles*/
import java.util.Scanner;
public class Convert{                                                             //Class
public static void main (String [] args) {                                        //Main Method
  Scanner myScanner = new Scanner (System.in);                                    //Construct Scanner input
  System.out.print("Enter the affected area in acres:");                          //Input
  double acreA = myScanner.nextDouble ();                                          //Recieve input and use it as a double
  System.out.print("Enter the amount of rainfall in the affected area:");         //Input
  double rain = myScanner.nextDouble();                                           //Recieve input and use it as a double
  
  double area = acreA *43560;          //Convert area in acres, to area in square feet
  double inA = area * Math.pow(12,2);  //Convert area in sqare feet, to area in square inches
  double inV = inA * rain;             //Multiply square inches by depth of rain in inches to get the volume in cubic inches
  double finA= inV/Math.pow(12,3);         //Convert cubic inches to cubic feet
  double miV = finA /Math.pow(5280,3); //Convert cubic feet to cubic miles
  
  System.out.println(miV + " cubic miles"); //Print out the volume of rain in cubic miles
  }//end of main method 
}//end of class