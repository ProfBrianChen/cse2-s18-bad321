/*Bradford DeMassa
CSE2 
9 February 2018
This program is a tip calculator that allows variable inputs by the user*/

import java.util.Scanner;                                                       //sets up scanner
public class Check {                                                            //standard class required for all java code
  public static void main (String[] args){
    
    Scanner myScanner = new Scanner ( System.in);                               //Constructs the scanner to recive input
    System.out.print("Enter the original cost of the check in the form xx.xx:");
    double checkCost = myScanner.nextDouble();                                  //Recieves input and stores it into a double
    System.out.print("Enter the perentage tip that you wish to pay as a whole number (in the form xx) :");
    double tipPercent = myScanner.nextDouble ();                                //Recives input and stores it into a double
    tipPercent /= 100;                                                          //We want to convert the perecentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner:");
    int numPeople = myScanner.nextInt();                                        //Recives input and puts it into an int value
    
    double totalCost;
    double costPerPerson;
    int dollars,                                                                //whole dollar amount of cost
          dimes, pennies;                                                       //for storing digits
                                                                                //to the right of the decimal point
                                                                                //for the cost$
    totalCost = checkCost * (1 + tipPercent);                                                    //calculates total bill including tip
    costPerPerson = totalCost / numPeople;                                                       //get the whole amount, dropping decimal fraction 
    dollars =(int) costPerPerson;                                                                //get dimes amount
    dimes= (int) (costPerPerson * 10) % 10;
    pennies= (int) (costPerPerson * 100) %10;
    System.out.println("Each person in the group owes $" + dollars +'.' + dimes + pennies);
  }     //end of main method
}       //end of class