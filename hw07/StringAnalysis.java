/*Bradford DeMassa
CSE2
27 March 2018
This program is meant to analyze parts
of a string. */
import java.util.Scanner;
public class StringAnalysis{
  public static void main (String[] args){
    Scanner input = new Scanner (System.in);//set up scanner
    int b = 0;
    while (b<1){
    String y = "yes";
    String n = "no";
    System.out.print("Enter a string:");
    String l = input.next();//string l
    System.out.print("Do you want to check all characters (yes/no):");
    String num = input.next();//answer stored
    if (num.equals(y)){
      boolean ans = Analysis (l);//method Analysis I
      if(ans == false){
        System.out.println("Error: Valid string not entered");
      }
      else{
        System.out.println("String is only letters.");
        b++;//increment b
      }
    }
    else if (num.equals(n)){
    System.out.print("How many characters are to be checked:");
    boolean x = input.hasNextInt();//if input is not a number
    while (x == false){
      String junk = input.next();//throw away junk number
      System.out.println("Error: Valid number not entered");
      System.out.print("Re-enter a valid number:");
      x = input.hasNextInt();//check for new input
    }
    int p = input.nextInt();//integers of characters to be checked
      boolean ans_2 = Analysis (l, p);
      if(ans_2 == false){
        System.out.println("Error: Valid string not entered");
      }
      else{
        System.out.println("String is only letters.");
        b++;//increment b
      }
    }
    else {
      System.out.println("Invalid answer entered.");
    }
    }//end of while
  }//end of main method
  public static boolean Analysis (String l){
    int length = l.length();//find string length
    for (int i=0; i<length; i++){
      char one = l.charAt(i);//create character one from character at position i
      if ('a'<= one && one <= 'z' || 'A'<= one && one<='Z'){}
      else{
        return false;
      }
    }//if for loop is completed, then there are only characters
    return true;
  }//End of Analysis I
  public static boolean Analysis (String l, int a){
    if (a> l.length()){
      a = l.length();//make the larger number equal to the length of the string
       for(int i=0; i<a; i++){
     char one = l.charAt(i);//character at position i
      if ('a'<= one && one <='z' || 'A'<= one && one<='Z'){}
      else{
        return false;
      }
         return true;
  }//all letters if the for loop is completed
      return false;
    }// if number is larger than the string
    else if (a<= l.length()){
    for(int i=0; i<a; i++){
     char one = l.charAt(i);
      if ('a'<= one && one <='z' || 'A'<= one && one<='Z'){}
      else{
        return false;
      }
  }//all characters are letters
      return true;
    }//if length is shorter than string
    return false;
  }//End of Analysis II
}//end of class 