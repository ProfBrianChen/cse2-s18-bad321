/* Bradford DeMassa
CSE2
27 March 2018
This program is asking the user for a shape and input
and will give the area of that shape and size the user
input.*/
import java.util.Scanner;
public class Area {
 public static void main (String[] args) {
   Scanner input = new Scanner (System.in);
   String t = "triangle";//strings
   String r = "rectangle";
   String c = "circle";
   
   String shape_f = Shape();
   if (shape_f.equals(t) || shape_f.equals(r)){
      System.out.print("Enter the base:");
   boolean x = input.hasNextDouble();//is input a double
   while (x==false){
     String junk = input.next();//throw away input
      System.out.println("Error: Valid number not entered.");
      System.out.print("Re-enter a valid number (double):");
      x = input.hasNextDouble();//new input
      }
   double base = input.nextDouble();//base double
  
   System.out.print("Enter the height:");
   boolean y = input.hasNextDouble();// is height a double
   while (y==false){
     String junk = input.next();//throw away input
      System.out.println("Error: Valid number not entered.");
      System.out.print("Re-enter a valid number (double):");
      y = input.hasNextDouble();//new input
      }
     double height = input.nextDouble();//height double
     if (!shape_f.equals(r)){
     double area = triangle(base, height);
     System.out.println("Area of the triangle is:" + area);
     }//if shape is not a rectangle it is a triangle
     else if (!shape_f.equals(t)){
     double area = rectangle(base, height);
     System.out.println("Area of the rectangle is:" + area);
     }//if shape is not a triangle it is a rectangle
   }//if shape is a triangle or rectangle
   if (shape_f.equals(c)){
   System.out.print("Enter the radius:");
   boolean z = input.hasNextDouble();//is input a double
   while (z==false){
     String junk = input.next();//throw away input
      System.out.println("Error: Valid number not entered.");
      System.out.print("Re-enter a valid number (double):");
      z = input.hasNextDouble();//new input
      }
     double radius = input.nextDouble();
     double area_c = circle(radius);
     System.out.println("Area of the circle is:"+ area_c);
   }//if the shape is a circle
   
 }//end main method
 public static String Shape (){
   Scanner input = new Scanner (System.in);
   System.out.print("Enter the shape (triangle, rectangle, or circle):");
   String shape_t = input.next();
   String t = "triangle";//strings
   String r = "rectangle";
   String c = "circle";
    int a = 0;
    while (a<1){
    if(shape_t.equals(t)){
      a++;
      return t;
    }//end of if shape is triangle
    else if(shape_t.equals(r)){
      a++;
      return r;
    }//end of 1st else if, if shape is rectangle
    else if(shape_t.equals(c)){
      a++;
      return c;
    }//end of 2nd else if, if shape is a circle
    else {
      String junk = shape_t;//throw away input
      System.out.println("Error: Valid shape not entered.");
      System.out.print("Re-enter a valid shape (triangle, rectangle, or circle):");
      shape_t = input.next();//new input
      }//end of else, if none of the above are true
    }//end of while method
   return c;//default place holder return statement
  }//end of shape method
 public static double triangle (double b, double h){
   return  b/2 * h;
   }//end of triangle
 public static double rectangle (double b, double h){
   return b * h;
 }//end of rectangle
 public static double circle (double r){
   return Math.PI * r * r;
 }
}//end class