/*Bradford DeMassa
CSE2
19 March 2018
This program is meant to create an Argyle pattern.
This will ask the user for various inputs such as
lengths and widths of the viewing window as well 
as what characters the image will be made out of */
import java.util.Scanner;
public class test_Argyle{
  public static void main (String[] args){
    Scanner input = new Scanner(System.in);//new scanner set up
    System.out.print("Please enter a positive integer for the width of the viewing window in characters:");
    boolean x = input.hasNextInt();
    while (x==false){
      String junk = input.next();//throw away input
      System.out.println("Error: Valid integer not entered.");
      System.out.print("Re-enter a valid integer:");
      x = input.hasNextInt();//new input
    }//while if input is not an integer
    int width = input.nextInt();//integer input
   
    System.out.print("Please enter a positive integer for the height of the viewing window in characters:");
    boolean y = input.hasNextInt();
    while (y==false){
      String junk = input.next();//throw away input
      System.out.println("Error: Valid integer not entered.");
      System.out.print("Re-enter a valid integer:");
      x = input.hasNextInt();//new input
    }//while if input is not an integer
    int height = input.nextInt();//integer input
    
    System.out.print("Please enter a positive integer for the width of the argyle diamonds:");
    boolean z = input.hasNextInt();
    while (z==false){
      String junk = input.next();//throw away input
      System.out.println("Error: Valid integer not entered.");
      System.out.print("Re-enter a valid integer:");
      z = input.hasNextInt();//new input
    }//while if input is not an integer
    int diamond = input.nextInt();//integer input
    
    System.out.print("Please enter a positive odd integer for the width of the argyle center stripe:");
    boolean a = input.hasNextInt();
    while (a==false){
      String junk = input.next();//throw away input
      System.out.println("Error: Valid integer not entered.");
      System.out.print("Re-enter a valid integer:");
      a = input.hasNextInt();//new input
    }//while if input is not an integer
    int stripe = input.nextInt();//integer input
    if (stripe%2 == 0){
      System.out.println("Error: Valid integer not entered.");
      System.out.print("Re-enter an odd integer:");
      x = input.hasNextInt();//check new input
    }
    
    System.out.print("Please enter a first character for the pattern fill:");
    String fill_1 = input.next();
    char result_1 = fill_1.charAt(0);
    
    System.out.print("Please enter a second character for the pattern fill:");
    String fill_2 = input.next();
    char result_2 = fill_2.charAt(0);  
    
    System.out.print("Please enter a third character for the stripe fill:");
    String fill_3 = input.next();
    char result_3 = fill_3.charAt(0);
    for (int i = 0 ; i<width; i++){
      for(int j = 0; j<height; j++){
       if(i==j){
         for(int k = 0; k<stripe; k++){
           System.out.print(result_3);
         }
       }
       else if (width-i==j+1){
         for(int l =0; l<stripe; l++){
           System.out.print(result_3);
         }
       }
       else if(width-i<=j+width/2+1 && 3*width/2-i >= j+1 && i<=j+width/2 && j<=i+width/2){
          System.out.print(result_1);//diamond
        }
       else{
         System.out.print(result_2);
       }
      }//inner for bracket
      System.out.println("");
   }//outer for bracket
  }//end of main method
}//end of class