/*
Bradford DeMassa
CSE 2
2 March 2018
This program is supposed to ask for information from the student about
their class schedule. Some details are like what time does their class
meet, or who the professor is, etc. If a wrong type is input, it is 
supposed to run an infinite loop until the user puts in the right type.*/

import java.util.Scanner;//import scanner class
public class Hw05{
  public static void main (String [] args){
   Scanner input = new Scanner(System.in);//Set up scanner class
   
   String department, name;//variables
   int crn, start, numOfClass, student;

   System.out.print("Enter the class CRN:");
   boolean a = input.hasNextInt();
   while (a==false){//is input not an integer
     String junk = input.next();//throw away input
     System.out.println("Error: Valid CRN not entered.");
     System.out.print("Re-enter a valid CRN:");
     a = input.hasNextInt();//is new input valid?
   }
   crn = input.nextInt();
   input.nextLine();//clear scanner
   
   System.out.print("Enter the department name:");
   boolean b = input.hasNextDouble();//is there a number entered
       while (b==true){//if so...
       String junk = input.next();//throw away input
       System.out.println("Error: Valid department not entered.");
       System.out.print("Re-enter a valid department:");
       b= input.hasNextDouble();//is there a number in the new input?
       }
      department = input.nextLine();
    
   System.out.print("Enter the number of classes per week:");
    boolean c = input.hasNextInt();//is there only numbers in the input?
    while (c==false){// if not...
      String junk = input.next();//throw away input
      System.out.println("Error: Valid number not entered.");
      System.out.print("Re-enter a valid number:");
      c= input.hasNextInt();//is new input valid?
      }
      numOfClass = input.nextInt();
    
    System.out.print("Enter the class start time (no colons);");
    boolean d = input.hasNextInt();//is the input an integer?
    while (d==false){// if not...
      String junk = input.next();
      System.out.println("Error: Valid time not entered.");
      System.out.print("Re-enter a valid start time:");
      d= input.hasNextInt();//is new input an integer?
      }
    start = input.nextInt();
    input.nextLine();//used to clear scanner
    
    System.out.print("Enter the instructors name:");
    boolean e = input.hasNextDouble();//is there numbers in the input
    while (e==true){// if so...
      String junk = input.next();// throw away input
      System.out.println("Error: Valid name not entered.");
      System.out.print("Re-enter the instructors name:");
      e = input.hasNextDouble();//is new input valid?
    }
    name = input.nextLine();
    
    System.out.print("Enter the number of students attending class:");
    boolean f = input.hasNextInt();//is input an integer
    while (f==false){// if not..
      String junk = input.next();// throw away input
      System.out.println("Error: Valid number not entered.");
      System.out.print("Re-enter the number of students attending class:");
      f = input.hasNextInt();// is new input valid?
    }    
    student = input.nextInt();
    
    System.out.println("");//Displayed information
    System.out.println("Class Registration Number:" + crn);
    System.out.println("Department name:" + department);
    System.out.println("Number of classes per week:" + numOfClass);
    System.out.println("Class start time:" + start);
    System.out.println("Instructor name:" + name);
    System.out.println("Number of students attending class:" + student);  
  }//end main method
}// end of class